<?php


//class OOP_practic
//{
//    public $name;
//    public $lastName;
//    public $age;
//    public $city;
//
//    public function getInfo()
//    {
//        echo "- User name: {$this->name}; <br> " . "- Surname: {$this->lastName}; <br> " . "- Your age: {$this->age}; <br> " . "- Your city: {$this->city}; <br> ";
//    }
//}
//
//$userOne = new OOP_practic();
//echo "Data about user:";
//echo "<br>";
//$userOne->name = "Roman";
//$userOne->lastName = "Ivanov";
//$userOne->age = 28;
//$userOne->city = "Kiev";
//$userOne->getInfo();
//echo "<br>";
//$userTwo = new OOP_practic();
//echo "Data about user:";
//echo "<br>";
//$userTwo->name = "Ivan";
//$userTwo->lastName = "Dobriy";
//$userTwo->age = 30;
//$userTwo->city = "Lviv";
//$userTwo->getInfo();
//echo "<br>";
//
//class User
//{
//    public $name;
//    public $password;
//    public $email;
//    public $city;
//
//    public function __construct($name, $city, $email, $password)
//    {
//        $this->name = $name;
//        $this->city = $city;
//        $this->email = $email;
//        $this->password = $password;
//    }
//
//
//    public function getInfo()
//    {
//        echo "{$this->name}<br>" . "{$this->city}<br>" . "{$this->email}<br>" . "{$this->password}<br>";
//    }
//}
//
//$user1 = new User("Stepan", "Dnipro", "Stepan@gmail.com", "Stepan_12345");
//$user1->getInfo();
//echo "<br>";
//
//class Destruct
//{
//    public function __construct()
//    {
//        $this->name = "Jane";
//        $this->lastName = "Robinson";
//        echo "Create constructor: {$this->name} " . "{$this->lastName}";
//        echo "<br>";
//    }
//
//    public function __destruct()
//    {
//        echo "Delete destructor: {$this->name} " . "{$this->lastName} ";
//    }
//}
//
//$obj = new Destruct();

//class User
//{
//    private $name = "Bodya";
//    private $lastName = "Ivashiv";
//    private $age = "36";
//    private $gender = "man";
//    private $city = "Lviv";
//
//    public function hello(){
//        echo  "Hello, ";
//    }
//
//    public function getInfo()
//    {
//        echo "Name: {$this->name}; " . "SurName: {$this->lastName}; " . "Age: {$this->age}; " .
//            "Gender: {$this->gender}; " . "City: {$this->city} ";
//    }
//}
//
//$user = new User();
//$user->hello();
//$user->getInfo();
//echo "<br>";
//echo "<br>";
//
//class laptop
//{
//    public function __construct($name, $cpu, $display, $model, $memory, $os)
//    {
//        $this->name = $name;
//        $this->cpu = $cpu;
//        $this->display = $display;
//        $this->model = $model;
//        $this->memory = $memory;
//        $this->os = $os;
//    }
//
//    public function getInfo()
//    {
//        echo "Data about laptop:<br>" .
//            " - Name: {$this->name} <br>" .
//                " - CPU: {$this->cpu} <br>" .
//                    " - Display: {$this->display} <br>" .
//                        " - Model: {$this->model} <br>" .
//                            " - Memory: {$this->memory} <br>" .
//                                " - OS: {$this->os} <br>";
//    }
//
//    public function __destruct()
//    {
//        echo "<br>";
//        echo "<br>";
//        echo "Delete data about laptop: {$this->memory}; " . "{$this->os}";
//    }
//}
//
//$laptop = new laptop("DELL","intel core i5","1920x1080","inspiron 15 7000 Gaming","8 GB","Windows 10");
//$laptop->getInfo();
//echo "<br>";
//
//Class staticClass{
//
//    private static $name1 = "Jack ";
//    private static $name2 = "Joe ";
//    private static $name3 = "Elizabet ";
//    private static $name4 = "John ";
//    private static $name5 = "Victoria ";
//
//    public static function getName(){
//        echo self::$name1;
//        echo self::$name2;
//        echo self::$name3;
//        echo self::$name4;
//        echo self::$name5;
//    }
//}
//staticClass::getName();

//inheritance
//class Animal
//{
//    public $name;
//    public $age;
//    public $eat;
//    public $walk;
//    public $poroda;
//    public $color;
//
//    public function helloWorld()
//    {
//        echo "Hello world!";
//    }
//}
//
//class dog extends Animal
//{
//    public function __construct($name, $age, $eat, $walk, $poroda, $color)
//    {
//        $this->name = $name;
//        $this->age = $age;
//        $this->eat = $eat;
//        $this->walk = $walk;
//        $this->poroda = $poroda;
//        $this->color = $color;
//    }
//
//    public function getInfo(){
//        echo " - Name dog: {$this->name} - <br>" .
//                    " - Age is: {$this->age} - <br>" .
//                        " - Eat: {$this->eat} - <br>" .
//                            " - Walk: {$this->walk} - <br>" .
//                                " - Poroda: {$this->poroda} - <br>" .
//                                    " - Color: {$this->color} - ";
//        echo  "<br>";
//    }
//}
//$animal = new Animal();
//$animal->helloWorld();
//echo "<br>";
//$dog = new dog("Jack","3 years","meat, milk, eggs","run","Haski","light");
//$dog->getInfo();
//echo "<br>";
//
//class cat extends Animal
//{
//    public $gender;
//
//    public function __construct($name, $age, $eat, $walk, $poroda, $color, $gender)
//    {
//       $this->name = $name;
//       $this->age = $age;
//       $this->eat = $eat;
//       $this->walk = $walk;
//       $this->poroda = $poroda;
//       $this->color = $color;
//       $this->gender = $gender;
//    }
//
//    public function getInfoCat()
//    {
//        echo " - My name is: {$this->name} - <br>" .
//                " - My age is: {$this->age} years old - <br>" .
//                    " - My eat: {$this->eat} - <br>" .
//                        " - I can: {$this->walk} - <br>" .
//                            " - My breed: {$this->poroda} - <br>" .
//                                " - My color: {$this->color} - <br>" .
//                                    " - I is: {$this->gender} - <br>";
//
//    }
//}
//$animal = new Animal();
//$animal->helloWorld();
//echo "<br>";
//$cat = new cat("Pusy","1,5","fish, milk, mouse","jump","Ukrainian Levkoy","gray","girl");
//$cat->getInfoCat();
//echo "<br>";
//
//// abstract classes
//abstract class college{
//    public $collegeName;
//    public $specialtyName;
//    public $studyAge;
//    public $numberPlace;
//
//    public function __construct($specialtyName, $studyAge, $numberPlace, $collegeName)
//    {
//        $this->collegeName = $collegeName;
//        $this->specialtyName = $specialtyName;
//        $this->studyAge = $studyAge;
//        $this->numberPlace = $numberPlace;
//    }
//
//    abstract function greet();
//}
//
//class PersonOne extends college{
//    public function greet()
//    {
//        echo "Hello world <br>";
//            echo "I am studying in: {$this->collegeName};<br> ";
//                echo "My specialty is: {$this->specialtyName};<br> ";
//                    echo "Study in college (age): {$this->studyAge};<br> ";
//                        echo "Number public places: {$this->numberPlace};<br> ";
//    }
//}
//
//class PersonTwo extends college{
//    public function greet()
//    {
//        echo "Hello world!<br>" . "I am studying: {$this->collegeName};<br>" .
//             "My specialty is: {$this->specialtyName};<br>" .
//             "Study in college (age): {$this->studyAge};<br>" .
//             "Number public places: {$this->numberPlace};<br>";
//    }
//}
//
//class PersonThree extends college{
//    public function greet()
//    {
//     echo "Hello world!<br>" . "I am studying in: {$this->collegeName};<br>" .
//          "My specialty is: {$this->specialtyName};<br>" .
//          "Study in college (age): {$this->studyAge};<br>" .
//          "Number public places: {$this->numberPlace};<br>";
//    }
//}
//
//$dataOfPersonOne = new PersonOne("Programmer","4 years","25","Zboriv college");
//$dataOfPersonOne->greet();
//echo "<br>";
//$dataOfPersonTwo = new PersonTwo("Mechanik","4 years","28","Technical college");
//$dataOfPersonTwo->greet();
//echo "<br>";
//$dataOfPersonThree = new PersonThree("Accountant","3 years","15","Galitskiy college");
//$dataOfPersonThree->greet();
//echo "<br>";
//
//// interface
//interface MyInterface{
//    public function myMethod1();
//}
//
//interface MyInterfaceTwo{
//    public function myMethod2();
//}
//
//interface MyInterfaceThree{
//    public function myMethod3();
//}
//
//class myData implements MyInterface, MyInterfaceTwo, MyInterfaceThree{
//    public function myMethod1()
//    {
//        echo "Hello world. ";
//    }
//
//    public function myMethod2()
//    {
//        echo "My name: Ihor. ";
//    }
//
//    public function myMethod3()
//    {
//        echo "I'm 18 years old. ";
//    }
//}
//$myData = new myData();
//$myData->myMethod1();
//$myData->myMethod2();
//$myData->myMethod3();
//echo "<br>";
//echo "<br>";
//
//// Traits
//Trait Person1{
//    public function PersonOne(){
//        echo "Hello, my name: Jack <br>";
//    }
//}
//
//Trait Person2{
//    public function PersonTwo(){
//        echo "Hello people. My name: Elizabet <br>";
//    }
//}
//
//Trait Person3{
//    public function PersonThree(){
//        echo "Hello world. My name: Jordan <br>";
//    }
//}
//
//class dataPeople{
//    use Person1, Person2, Person3;
//}
//
//$obj = new dataPeople();
//$obj->PersonOne();
//$obj->PersonTwo();
//$obj->PersonThree();
//echo "<br>";
//
////constants in classes
//
//class jobConst{
//    const NAMING = "My name class";
//        public function firstClass(){
//            echo self::NAMING;
//        }
//}
//
//$w = new jobConst();
//$w->firstClass();
//echo "<br>";
//echo jobConst::NAMING;
//echo "<br>";
//
//// static methods
//class Hyvor{
//    public static function myMethod(){
//        echo "Hyvor, Inc ";
//    }
//
//    public function nameCompany(){
//        self::myMethod();
//    }
//}
//echo "<br>";
//$company = new Hyvor();
//$company->nameCompany();
//echo Hyvor::myMethod();

class Family
{
    public $name;
    public $lastName;
    public $listen;
    public $speak;
    public $see;
    public $eat;
    public $walk;
    public $hobby;

    public function greet()
    {
        echo "Hello. ";
    }
}

class grandFather extends Family
{
    public function __construct($name, $lastName, $listen, $speak, $see, $eat, $walk, $hobby)
    {
        $this->name = $name;
        $this->lastName = $lastName;
        $this->listen = $listen;
        $this->speak = $speak;
        $this->see = $see;
        $this->eat = $eat;
        $this->walk = $walk;
        $this->hobby = $hobby;
    }

    public function getInfoGrandFather(){
        echo "My name: {$this->name}<br>" .
             "My surname: {$this->lastName}<br>" .
             "My favourite music or artist is: {$this->listen}<br>" .
             "I can: {$this->speak}<br>" .
             "I see: {$this->see}<br>" .
             "I eat: {$this->eat}<br>" .
             "I don't often: {$this->walk}<br>" .
             "My favorite hobby: {$this->hobby}<br>";
    }
}

class grandMother extends Family
{
    public function __construct($name, $lastName, $listen, $speak, $see, $eat, $walk, $hobby)
    {
        $this->name = $name;
        $this->lastName = $lastName;
        $this->listen = $listen;
        $this->speak = $speak;
        $this->see = $see;
        $this->eat = $eat;
        $this->walk = $walk;
        $this->hobby = $hobby;
    }

    public function getInfoGrandMother(){
        echo "My name: {$this->name}<br>" .
             "Surname: {$this->lastName}<br>" .
             "My favourite music or artist is: {$this->listen}<br>" .
             "I can: {$this->speak}<br>" .
             "I see: {$this->see}<br>" .
             "I eat: {$this->eat}<br>" .
             "I often: {$this->walk}<br>" .
             "My favorite hobby: {$this->hobby}<br>";
    }
}

class Father extends Family
{
    public function __construct($name, $lastName, $listen, $speak, $see, $eat, $walk, $hobby)
    {
        $this->name = $name;
        $this->lastName = $lastName;
        $this->listen = $listen;
        $this->speak = $speak;
        $this->see = $see;
        $this->eat = $eat;
        $this->walk = $walk;
        $this->hobby = $hobby;
    }

    public function getInfoFather(){
        echo "My name: {$this->name}<br>" .
            "Surname: {$this->lastName}<br>" .
            "My favourite music or artist is: {$this->listen}<br>" .
            "I can: {$this->speak}<br>" .
            "I see: {$this->see}<br>" .
            "I eat: {$this->eat}<br>" .
            "I often: {$this->walk}<br>" .
            "My favorite hobby: {$this->hobby}<br>";
    }
}

class Mother extends Family
{
    public function __construct($name, $lastName, $listen, $speak, $see, $eat, $walk, $hobby)
    {
        $this->name = $name;
        $this->lastName = $lastName;
        $this->listen = $listen;
        $this->speak = $speak;
        $this->see = $see;
        $this->eat = $eat;
        $this->walk = $walk;
        $this->hobby = $hobby;
    }

    public function getInfoMother(){
        echo "My name: {$this->name}<br>" .
            "Surname: {$this->lastName}<br>" .
            "My favourite music or artist is: {$this->listen}<br>" .
            "I can: {$this->speak}<br>" .
            "I see: {$this->see}<br>" .
            "I eat: {$this->eat}<br>" .
            "I often: {$this->walk}<br>" .
            "My favorite hobby: {$this->hobby}<br>";
    }
}

class SonOne extends Family
{
    public function __construct($name, $lastName, $listen, $speak, $see, $eat, $walk, $hobby)
    {
        $this->name = $name;
        $this->lastName = $lastName;
        $this->listen = $listen;
        $this->speak = $speak;
        $this->see = $see;
        $this->eat = $eat;
        $this->walk = $walk;
        $this->hobby = $hobby;
    }

    public function getInfoSonOne(){
        echo "My name: {$this->name}<br>" .
            "Surname: {$this->lastName}<br>" .
            "My favourite music or artist is: {$this->listen}<br>" .
            "I can: {$this->speak}<br>" .
            "I see: {$this->see}<br>" .
            "I eat: {$this->eat}<br>" .
            "I often: {$this->walk}<br>" .
            "My favorite hobby: {$this->hobby}<br>";
    }
}

class SonTwo extends Family
{
    public function __construct($name, $lastName, $listen, $speak, $see, $eat, $walk, $hobby)
    {
        $this->name = $name;
        $this->lastName = $lastName;
        $this->listen = $listen;
        $this->speak = $speak;
        $this->see = $see;
        $this->eat = $eat;
        $this->walk = $walk;
        $this->hobby = $hobby;
    }

    public function getInfoSonTwo(){
        echo "My name: {$this->name}<br>" .
            "Surname: {$this->lastName}<br>" .
            "My favourite music or artist is: {$this->listen}<br>" .
            "I can: {$this->speak}<br>" .
            "I see: {$this->see}<br>" .
            "I eat: {$this->eat}<br>" .
            "I often: {$this->walk}<br>" .
            "My favorite hobby: {$this->hobby}<br>";
    }
}

$family = new Family();
$family->greet();
$grandFather = new grandFather("Misha","Zagorodniy","folk music","speak with people","all world","different eat","walk","mastering");
$grandFather->getInfoGrandFather();
echo "<br>";

$family->greet();
$grandMother = new grandMother("Yaroslava","Zagorodna","folk music","speak with people","all world","different eat","walk","wear newspaper");
$grandMother->getInfoGrandMother();
echo "<br>";

$family->greet();
$father = new Father("Vova","Krysiuk","Oleh Vinik","speak with people","all world","different eat","walk","play (football, volleyball, Tennis, read books and other)");
$father->getInfoFather();
echo "<br>";

$family->greet();
$mother = new Mother("Oksana","Krysiuk","modern music","speak with people","all world","different eat","walk","cookie, mastering, sew and other");
$mother->getInfoMother();
echo "<br>";

$family->greet();
$sonOne = new SonOne("Kolya","Krysiuk","rock and rep","speak with people","all world","different eat","walk","play (football, volleyball, reads book and other)");
$sonOne->getInfoSonOne();
echo "<br>";

$family->greet();
$sonTwo = new SonTwo("Ihor","Krysiuk","rock, rep and classic","speak with people","all world","different eat","walk","play (football, basketball, Tennis, reads book and other)");
$sonTwo->getInfoSonTwo();
